function DisconnectEyetracker(socket, endpoint, ctx)
zmq.core.disconnect(socket, endpoint);
zmq.core.close(socket);
zmq.core.ctx_shutdown(ctx);
zmq.core.ctx_term(ctx);