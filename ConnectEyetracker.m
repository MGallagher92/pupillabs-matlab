function [socket, endpoint, ctx] = ConnectEyetracker(endpoint)
% Pupil Remote address
if nargin < 1 % Just in case the address ever changes??
endpoint =  'tcp://127.0.0.1:50020';
end
% Setup zmq context and remote helper
ctx = zmq.core.ctx_new();
socket = zmq.core.socket(ctx, 'ZMQ_REQ');
% set timeout to 1000ms in order to not get stuck in a blocking
% mex-call if server is not reachable, see
% http://api.zeromq.org/4-0:zmq-setsockopt#toc19
zmq.core.setsockopt(socket, 'ZMQ_RCVTIMEO', 1000);
fprintf('Connecting to %s\n', endpoint);
zmq.core.connect(socket, endpoint);
tic; % Measure round trip delay
zmq.core.send(socket, uint8('t'));
result = zmq.core.recv(socket);
fprintf('%s\n', char(result));
fprintf('Round trip command delay: %s\n', toc);
