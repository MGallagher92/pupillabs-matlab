%% Start eye tracker recording
% MG Feb 2021
% This will first synchronise the time and start the eye tracker recording.
function StartRecordEyetracker(socket)
% set current Pupil time to 0.0
zmq.core.send(socket, uint8('T 0.0'));
result = zmq.core.recv(socket);
fprintf('%s\n', char(result));

% start recording
zmq.core.send(socket, uint8('R'));
result = zmq.core.recv(socket);
fprintf('Recording should start: %s\n', char(result));