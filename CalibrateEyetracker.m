function CalibrateEyetracker(socket)
zmq.core.send(socket, uint8('C')); % Tell PupilCapture to enter calibration mode
result = zmq.core.recv(socket);
fprintf('%s\n', char(result));