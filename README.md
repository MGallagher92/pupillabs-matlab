# README #

Hello! In the AV lab at Cardiff University we are using a PupilLabs Core eyetracker to track eye position during active head movements. 
The PupilLabs GUI is great and very simple to use, but we encountered a few small issues when trying to integrate everything with MATLAB. 
I can't guarantee that any of the solutions and codes I post here will definitely work with your system, but this is a quick guide with the steps that we took to solve the problems. 
We are using Windows 10, MATLAB 2018a, Visual Studio 2017 and ZeroMQ 4.3.2.
Honestly, this ReadMe is likely to be the most helpful bit over the actual functions!

# Initial Setup #

## MATLAB files ##
PupilLabs have supplied a Git repository containing the basics to connect to the eyetracker. You can find that here -> https://github.com/pupil-labs/pupil-helpers/tree/master/matlab

You will also need to download the MATLAB package for ZeroMQ  -> https://github.com/fagg/matlab-zmq

And you will need the message pack -> https://github.com/bastibe/matlab-msgpack

## Software ##
You need to install the main PupilCore GUI -> https://docs.pupil-labs.com/core/

You will also need some version of Microsoft Visual Studio -> https://visualstudio.microsoft.com/

And ZeroMQ -> https://zeromq.org/download/
-NOTE: The location of these libraries is critical for the codes to work, but none of the error codes tell you that in an obvious way! 

Going to assume you already have MATLAB!

## File Locations ##
We encountered a world of problems when folders weren't in the right places, so this is what my folder structure looks like. It may differ for you, but if you get incomprehensible errors, it's worth trying to move things around. These folders and subfolders need to be on the MATLAB path for the later functions to work. 

1. Main folder containing:
	1. All the MATLAB functions
	2. Folder for the MATLAB Message Pack
	3. Folder for the MATLAB ZMQ Pack containing:
		* Config MATLAB scripts and Make.m
		* Examples folder
		* SRC folder
		* Tests folder
		* Lib folder containing:	
			* +ZMQ Folder containing:
				* @Context folder			
				* @Socket folder			
				* __IMPORTANT: the libzmq folder you downloaded and unzipped earlier!__			
				* +core folder containing:			
					* Eventually the MEX files once they have been generated				
					* __ IMPORTANT: A copy of libzmq-v141-mt-4_3_2.dll; libzmq-v141-mt-4_3_2.lib; libzmq-v141-mt-s-4_3_2.lib; and libsodium.dll__ These files are already in the libzmq folder, but we found that the libraries need to be in both folders, even if all the folders are on the MATLAB path! 

# Configure MEX Files #

Once you've added everything to the above folders, you can start to generate the mex files. 

First, make sure MATLAB is configured to use your version of Visual Studio by running the command


```
mex -setup
```

Then, open config.m in ./Main folder/MATLAB ZMQ/, and change the paths to:


```
% ZMQ library filename
ZMQ_COMPILED_LIB = 'libzmq-v141-mt-4_3_2.lib';

% ZMQ library path
ZMQ_LIB_PATH = 'C:\Your Username Here\Main Folder\ZMQ Pack Folder\lib\+zmq\libzmq';

% ZMQ headers path
ZMQ_INCLUDE_PATH = 'C:\Your Username Here\Main Folder\ZMQ Pack Folder\lib\+zmq\libzmq';
```


From there, you can open and run the make.m script from your main folder, and this should generate your mex files. If you have problems here, verify that your folder structure is correct, and that you have definitely configured your mex setup properly. You should only need to run make.m once!

# Test Link with PupilLabs Software #

Once you've generated your mex files (this was the hardest part for us!), you can try the pupil_remote_control.m example script. Open the Pupil Capture software and run the MATLAB script. It should connect to the eyetracker, sync the time, start and stop the recording. If this works, you're good to try all the other functions! 