function StopRecordEyetracker(socket)
zmq.core.send(socket, uint8('r')); % Tell PupilCapture to stop
result = zmq.core.recv(socket); % Tell MATLAB what happened
fprintf('Recording stopped: %s\n', char(result)); % Tell us what happened
