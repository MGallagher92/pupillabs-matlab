function SendTriggerEyetracker(socket, annotation, duration)
if nargin < 3
    duration = 0;
end

%% Get the pupil time
zmq.core.send(socket, uint8('t')); % Ask what pupil time is
resultT = zmq.core.recv(socket); % Tell MATLAB what pupil time is
time = str2double(char(resultT)); % Pupil sends time as uint8, we convert to character to get time, then to double

%% Send the annotation
% message is a containers.Map object containing:
% topic = annotation.String (String can be any word)
% label = the annotation message (e.g., Trial 1, start, stop)
% timestamp = the time the annotation was sent 
% duration = how long the annotation lasts (can be 0)
message = containers.Map({'topic', 'label', 'timestamp', 'duration'},{'annotation.String', annotation, time, duration}); 
% send_annotation(socket, message) 

% Send the annotation
payload = dumpmsgpack(message); % converts container.Map to somethig pupil will understand
zmq.core.send(socket, uint8(message('topic')), 'ZMQ_SNDMORE'); % send the topic
zmq.core.send(socket, payload); % send the message

result = zmq.core.recv(socket); % check message was sent 
fprintf('Annotation: %s\n', char(result)) % tell us message was sent